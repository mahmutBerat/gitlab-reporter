package com.mbi.issuetracker.restapi.webhook;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest

public class WebHookAPITest {

    @Autowired
    WebHookAPI webhookAPI;

    @Test
    public void commentHookTest() {
        webhookAPI.commentEvents();
    }
}