package com.mbi.issuetracker.restapi.webhook.service;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.service.GitlabIssuesService;
import com.mbi.issuetracker.service.GitlabUserService;
import com.mbi.issuetracker.websocket.service.WebHookSocketService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.LinkedHashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = WebHookIssueService.class)
@MockBean(classes = {WebHookSocketService.class,
        GitlabIssuesService.class, GitlabUserService.class})
@RunWith(SpringRunner.class)
public class WebHookIssueServiceTest {

    @Autowired
    WebHookIssueService webhookIssueService;

    @MockBean
    GitlabUserService gitlabUserService;

    @MockBean
    GitlabIssuesService issuesService;

    private String newIssueEvent = "{\"object_kind\":\"issue\",\"event_type\":\"issue\",\"user\":{\"name\":\"developer 1 lastname 1\",\"username\":\"developer_1\",\"avatar_url\":\"https://www.gravatar.com/avatar/6350fff222f833cab6521bc545a7a081?s=80&d=identicon\"},\"project\":{\"id\":3,\"name\":\"Gitlab project 1\",\"description\":\"My first gitlab project description \",\"web_url\":\"http://gitlab.example.com:30080/developer_1/newproject\",\"avatar_url\":null,\"git_ssh_url\":\"ssh://git@gitlab.example.com:30022/developer_1/newproject.git\",\"git_http_url\":\"http://gitlab.example.com:30080/developer_1/newproject.git\",\"namespace\":\"developer_1\",\"visibility_level\":20,\"path_with_namespace\":\"developer_1/newproject\",\"default_branch\":null,\"ci_config_path\":null,\"homepage\":\"http://gitlab.example.com:30080/developer_1/newproject\",\"url\":\"ssh://git@gitlab.example.com:30022/developer_1/newproject.git\",\"ssh_url\":\"ssh://git@gitlab.example.com:30022/developer_1/newproject.git\",\"http_url\":\"http://gitlab.example.com:30080/developer_1/newproject.git\"},\"object_attributes\":{\"author_id\":2,\"closed_at\":null,\"confidential\":false,\"created_at\":\"2019-05-12 15:02:45 UTC\",\"description\":\"adasda\",\"due_date\":null,\"id\":42,\"iid\":39,\"last_edited_at\":null,\"last_edited_by_id\":null,\"milestone_id\":null,\"moved_to_id\":null,\"project_id\":3,\"relative_position\":1073761323,\"state\":\"opened\",\"time_estimate\":0,\"title\":\"Web Hook Issue 1\",\"updated_at\":\"2019-05-12 15:02:45 UTC\",\"updated_by_id\":null,\"url\":\"http://gitlab.example.com:30080/developer_1/newproject/issues/39\",\"total_time_spent\":0,\"human_total_time_spent\":null,\"human_time_estimate\":null,\"assignee_ids\":[],\"assignee_id\":null,\"action\":\"open\"},\"labels\":[],\"changes\":{\"author_id\":{\"previous\":null,\"current\":2},\"created_at\":{\"previous\":null,\"current\":\"2019-05-12 15:02:45 UTC\"},\"description\":{\"previous\":null,\"current\":\"adasda\"},\"id\":{\"previous\":null,\"current\":42},\"iid\":{\"previous\":null,\"current\":39},\"project_id\":{\"previous\":null,\"current\":3},\"relative_position\":{\"previous\":null,\"current\":1073761323},\"state\":{\"previous\":null,\"current\":\"opened\"},\"title\":{\"previous\":null,\"current\":\"Web Hook Issue 1\"},\"updated_at\":{\"previous\":null,\"current\":\"2019-05-12 15:02:45 UTC\"},\"total_time_spent\":{\"previous\":null,\"current\":0}},\"repository\":{\"name\":\"Gitlab project 1\",\"url\":\"ssh://git@gitlab.example.com:30022/developer_1/newproject.git\",\"description\":\"My first gitlab project description \",\"homepage\":\"http://gitlab.example.com:30080/developer_1/newproject\"}}";

    @Test
    public void shouldHandleFirstIssueCreationObject() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        LinkedHashMap incomingObject = objectMapper.readValue(newIssueEvent, LinkedHashMap.class);

        ReporterUser reporterUser = new ReporterUser();
        reporterUser.setUserId(2L);
        when(gitlabUserService.getUserByUserId(anyLong())).thenReturn(reporterUser);
        when(issuesService.findIssueByIssueId(anyInt())).thenReturn(null);

        webhookIssueService.inspectIncomingIssueObject(incomingObject);
        verify(issuesService, times(1)).saveAndFlushReporterIssue(any());
    }

}