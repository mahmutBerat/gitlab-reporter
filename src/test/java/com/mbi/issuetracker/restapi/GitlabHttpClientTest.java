package com.mbi.issuetracker.restapi;

import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import com.mbi.issuetracker.service.GitlabAuthService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GitlabHttpClientTest {
    @Autowired
    GitlabAuthService authService;

    @Test
    public void shouldGetAccessToken(){
        String code = "f0f606471bc6ad018ff32c438fabd6dc5f31ef7961054b82f6726a910951b262";
        AccessTokenResponseDTO accessTokenRequester = authService.postAuthorization(code);
        Assert.assertNotNull(accessTokenRequester);
    }

}