package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.IssueOperationStatus;
import com.mbi.issuetracker.persistence.IssueOperationType;
import com.mbi.issuetracker.persistence.entity.IssueOperations;
import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.IssueDao;
import com.mbi.issuetracker.persistence.repository.IssueOperationsDao;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.restapi.factory.ReporterIssueFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = {GitlabIssueOperationService.class, ReporterIssueFactory.class, PasswordEncoder.class, GitlabFactory.class, GitlabUserService.class, GitlabIssuesService.class})
@RunWith(SpringRunner.class)
@DataJpaTest
@EnableJpaRepositories("com.example.issuetracker.persistence")
@EntityScan("com.example.issuetracker.persistence")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class GitlabIssueOperationServiceTest {

    @Autowired
    GitlabIssueOperationService operationService;

    @Autowired
    IssueDao issueDao;

    @Autowired
    IssueOperationsDao issueOperationsDao;

    @MockBean
    GitlabUserService userService;

    @MockBean
    GitlabIssuesService gitlabIssuesService;

    Long userId = 10L;
    ReporterUser mockUser = mock(ReporterUser.class);
    IssueDTO issueDTO;

    @Before
    public void init() {
        when(mockUser.getId()).thenReturn(userId);
        when(mockUser.getUserId()).thenReturn(userId);
        when(userService.getUserByUserId(userId)).thenReturn(mockUser);
        int projectId = 1;
        int issueId = 0;
        String description = "Test Description";
        String title = "Test Title";

        issueDTO = new IssueDTO(projectId, issueId, description, title, userId);
    }

    @Test
    public void testFirstCreateIssue() {
        //1 persist to reporter issue
        ReporterIssue reporterIssue = operationService.createReporterIssue(issueDTO);
        assertThat(reporterIssue.getDescription(), is(issueDTO.getDescription()));
        assertThat(reporterIssue.getTitle(), is(issueDTO.getTitle()));
        assertThat(reporterIssue.getProjectId(), is(issueDTO.getProjectId()));
        assertThat(reporterIssue.getCreatedBy().getId(), is(userId));
        assertThat(reporterIssue.getCreatedAt(), notNullValue());


        //2 persist to operation issue with WAITING state
        IssueOperations issueOperation = operationService.addCreateOperation(reporterIssue);
        assertThat(issueOperation.getType(), is(IssueOperationType.CREATE));
        assertThat(issueOperation.getStatus(), is(IssueOperationStatus.WAITING));
        assertThat(issueOperation.getCreation_date(), notNullValue());
        assertThat(issueOperation.getReporterIssue().getProjectId(), is(issueDTO.getProjectId()));
        assertThat(issueOperation.getId(), greaterThan(0L));
        assertThat(issueOperation.getSubmitter(), is(mockUser));

    }

    @Test
    public void shouldReturnWaitingOperations() {
        IssueOperations operations = new IssueOperations();
        IssueOperations operations_1 = new IssueOperations();
        IssueOperations operations_2 = new IssueOperations();
        IssueOperations operations_3 = new IssueOperations();
        IssueOperations operations_4 = new IssueOperations();
        operations.setStatus(IssueOperationStatus.SUCCESS);
        operations.setCreation_date(new Date(12345));
        operations_1.setStatus(IssueOperationStatus.FAIL);
        operations_1.setCreation_date(new Date(123456));
        operations_2.setStatus(IssueOperationStatus.WAITING);
        operations_2.setCreation_date(new Date(123457));
        operations_3.setStatus(IssueOperationStatus.WAITING);
        operations_3.setCreation_date(new Date(123458));
        operations_4.setStatus(IssueOperationStatus.WAITING);
        operations_4.setCreation_date(new Date(123459));
        issueOperationsDao.saveAndFlush(operations);
        issueOperationsDao.saveAndFlush(operations_1);
        issueOperationsDao.saveAndFlush(operations_2);
        issueOperationsDao.saveAndFlush(operations_3);
        issueOperationsDao.saveAndFlush(operations_4);

        List<IssueOperations> issueOperationsList = operationService.findWaitingOperations();
        assertThat(issueOperationsList.isEmpty(), is(false));
        assertThat(issueOperationsList.size(), is(3));
        assertThat(issueOperationsList.get(0).getCreation_date(), is(operations_2.getCreation_date()));
        assertThat(issueOperationsList.get(1).getCreation_date(), is(operations_3.getCreation_date()));
        assertThat(issueOperationsList.get(2).getCreation_date(), is(operations_4.getCreation_date()));

    }

//    @Test
    public void testAddCloseOperation(){
        operationService.addCreateOperation(new ReporterIssue());
    }

    @After
    public void tearDown() {

    }
}