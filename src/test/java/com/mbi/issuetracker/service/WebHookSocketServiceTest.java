package com.mbi.issuetracker.service;

import com.mbi.issuetracker.websocket.service.WebHookSocketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@MockBean(classes = {GitlabUserService.class})
public class WebHookSocketServiceTest {

    @Autowired
    WebHookSocketService webhookSocketService;

    @Test
    public void shouldSendMessage(){
        webhookSocketService.sendCommentEvent("test message content");
        assertThat(2,greaterThan(1));
    }

}