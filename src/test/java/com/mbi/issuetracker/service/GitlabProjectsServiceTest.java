package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


//TODO Complete these test cases
@SpringBootTest(classes = GitlabProjectsService.class)
@RunWith(SpringRunner.class)
@MockBean(classes = {GitlabUserService.class})
public class GitlabProjectsServiceTest {

    @Autowired
    GitlabProjectsService gitlabProjectsService;

    @MockBean
    GitlabUserService gitlabUserService;

    @MockBean
    GitlabFactory gitlabFactory;


    public void init(){

    }

    @Test
    public void shouldFetchUserProjectsByName() throws GitLabApiException {
        String name = "developer_1";
        gitlabProjectsService.fetchUserProjects(name);
    }

    @Test
    public void shouldFetchUserProjectsById() throws GitLabApiException {
        long id = 1L;
        ReporterUser reporterUser = new ReporterUser();
        when(gitlabUserService.getUser(any())).thenReturn(reporterUser);
        String testToken = "testToken";
        GitLabApi gitLabTestApi = new GitLabApi(testToken,"");
        when(gitlabFactory.createAPIByBearerToken(anyString())).thenReturn(gitLabTestApi);


        gitlabProjectsService.fetchUserProjects(id);

    }
}