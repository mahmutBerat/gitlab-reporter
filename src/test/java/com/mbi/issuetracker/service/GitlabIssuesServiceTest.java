package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterProject;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.IssueDao;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.security.SecurityPrincipal;
import com.mbi.issuetracker.websocket.WebSocketConfig;
import org.gitlab4j.api.models.Issue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@ContextConfiguration(classes = {GitlabIssuesService.class, GitlabFactory.class, WebSocketConfig.class})
@RunWith(SpringRunner.class)
@DataJpaTest
//@EnableAutoConfiguration
//@EnableTransactionManagement
@EnableJpaRepositories("com.example.issuetracker.persistence")
@EntityScan("com.example.issuetracker.persistence")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@EnableAutoConfiguration(exclude=AutoConfigureTestDatabase.class)
@ActiveProfiles("test")
public class GitlabIssuesServiceTest {

    @Autowired
    IssueDao issueDao;

    @Autowired
    GitlabIssuesService gitlabIssuesService;

    @Autowired
    private TestEntityManager entityManager;

    ReporterProject reporterProject;
    ReporterUser reporterUser;
    SecurityPrincipal securityPrincipal;

    ReporterIssue failedIssue1 = new ReporterIssue();
    ReporterIssue failedIssue2 = new ReporterIssue();

    @Before
    public void init() {
        reporterProject = new ReporterProject();
        reporterProject.setProjectId(3);
        reporterProject.setName("testProject");

        reporterUser = new ReporterUser();
        reporterUser.setBearerToken("7e1b0b0471211aacc7535231042ac248c67f69bd740040af024c60b2281e483d");
        securityPrincipal = new SecurityPrincipal(reporterUser);

        failedIssue1.setId(100);
        failedIssue1.setFailedToSent(Boolean.TRUE);
        failedIssue1.setFailedDate(new Date());

        failedIssue2.setId(101);
        failedIssue2.setFailedToSent(Boolean.TRUE);
        failedIssue2.setFailedDate(new Date());
    }

    @Test
    public void shouldFetchProjectIssues() {
        //TODO refactor parameter of fetchIssues to SecurityPrinciple
        List<Issue> issueList = gitlabIssuesService.fetchIssues(reporterUser, reporterProject.getProjectId());
        assertThat(issueList.isEmpty(), is(false));
    }

    public void shouldCheckFailedIssuesQuery() {
        persistSomeFailedIssues();
        List<ReporterIssue> failedIssues = gitlabIssuesService.getFailedIssues();
        assertThat(failedIssues.size(), is(2));
    }

    private void persistSomeFailedIssues() {
        issueDao.saveAndFlush(failedIssue1);
        issueDao.saveAndFlush(failedIssue2);
    }
}