package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterNote;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.NoteDao;
import com.mbi.issuetracker.restapi.dto.NoteDTO;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.security.SecurityPrincipal;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.NotesApi;
import org.gitlab4j.api.models.Note;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {GitlabNotesService.class})
@SpringBootTest(classes = {
        GitlabNotesService.class,
        NoteDao.class,
        GitlabFactory.class
})
@RunWith(SpringRunner.class)
public class GitlabNotesServiceTest {

    @Autowired
    GitlabNotesService gitlabNotesService;

    @MockBean
    NoteDao noteDao;

    @MockBean
    GitlabFactory gitlabFactory;

    @Mock
    GitLabApi gitLabApi;
    @Mock
    NotesApi notesApi;

    private ReporterUser reporterUser;
    private int projectId = 3;
    private int issueId = 2;

    @Before
    public void init() {
        reporterUser = new ReporterUser();
        reporterUser.setBearerToken("7e1b0b0471211aacc7535231042ac248c67f69bd740040af024c60b2281e483d");
        SecurityPrincipal securityPrincipal = new SecurityPrincipal(reporterUser);

        when(gitlabFactory.createAPIByBearerToken(anyString())).thenReturn(gitLabApi);
        when(gitLabApi.getNotesApi()).thenReturn(notesApi);
    }

    @Test
    public void shouldFetchIssueNotes() throws GitLabApiException {
        //TODO check these variables

        Note note = new Note();
        note.setTitle("Test Mock Note");
        List<Note> noteList = new ArrayList<>();
        noteList.add(note);

        when(notesApi.getIssueNotes(any(Integer.class), any(Integer.class))).thenReturn(noteList);

        //TODO refactor parameter to SecurityPrinciple
        List<ReporterNote> fetchIssueNotes = gitlabNotesService.fetchIssueNotes("", projectId, issueId);
        assertThat(fetchIssueNotes.isEmpty(), is(false));
        assertThat(fetchIssueNotes.get(0).getTitle(), is(note.getTitle()));
        verify(noteDao, times(1)).save(any(ReporterNote.class));
    }


    @Test
    public void shouldSaveNote() throws GitLabApiException {
        Note note = new Note();
        note.setId(2);
        note.setTitle("Test Mock Note");
        NoteDTO noteDTO = new NoteDTO(projectId, issueId, "test body content",0);
        when(notesApi.createIssueNote(any(Integer.class), any(Integer.class), any(String.class))).thenReturn(note);
        ReporterNote reporterNote = gitlabNotesService.createNote(noteDTO, reporterUser.getBearerToken());

        assertThat(reporterNote.getTitle(), is(note.getTitle()));
        assertThat(reporterNote.getId(), is(note.getId()));
    }

}