package com.mbi.issuetracker.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private DateUtil() {
        throw new IllegalArgumentException("Shall not initialize an util class ");
    }

    public static Date convertToNewFormat(String dateStr) {
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss 'UTC'");
        sourceFormat.setTimeZone(utc);
        Date convertedDate = null;
        try {
            convertedDate = sourceFormat.parse(dateStr);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return convertedDate;
    }

}
