package com.mbi.issuetracker.restapi.dto;

public class AccessTokenDTO {
    private String status;

    public AccessTokenDTO(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
