package com.mbi.issuetracker.restapi.dto;

public class NoteDTO {

    private int noteId;

    private int projectId;

    private int issueId;

    private String description;

    public NoteDTO() {
    }

    public NoteDTO(int projectId, int issueId, String description, int noteId) {
        this.projectId = projectId;
        this.issueId = issueId;
        this.description = description;
        this.noteId = noteId;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "NoteDTO{" +
                "projectId=" + projectId +
                ", issueId=" + issueId +
                ", description=" + description +
                '}';
    }
}
