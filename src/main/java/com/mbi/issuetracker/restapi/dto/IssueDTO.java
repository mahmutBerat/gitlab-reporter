package com.mbi.issuetracker.restapi.dto;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;

public class IssueDTO {

    private int reporterId = -1;

    private int projectId;

    private int issueId;

    private String description;

    private String title;

    private Long userId;

    private boolean isFailed;

    public IssueDTO() {
    }

    public IssueDTO(ReporterIssue reporterIssue) {
        this.projectId = reporterIssue.getProjectId();
        this.description = reporterIssue.getDescription();
        this.title = reporterIssue.getTitle();
        this.reporterId = reporterIssue.getReporterId();
        this.isFailed = reporterIssue.getFailedToSent();
    }

    public static IssueDTO of(ReporterIssue reporterIssue) {
        if (reporterIssue != null) {
            return new IssueDTO(reporterIssue);
        }
        return null;
    }

    public IssueDTO(int projectId, String description, String title) {
        this.projectId = projectId;
        this.description = description;
        this.title = title;
    }

    public IssueDTO(int projectId, int issueId) {
        this.projectId = projectId;
        this.issueId = issueId;
    }

    public IssueDTO(int projectId, int issueId, String description, String title) {
        this.projectId = projectId;
        this.issueId = issueId;
        this.description = description;
        this.title = title;
    }

    public IssueDTO(int projectId, int issueId, String description, String title, Long userId) {
        this.projectId = projectId;
        this.issueId = issueId;
        this.description = description;
        this.title = title;
        this.userId = userId;
    }

    public int getReporterId() {
        return reporterId;
    }

    public void setReporterId(int reporterId) {
        this.reporterId = reporterId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isFailed() {
        return isFailed;
    }

    public void setFailed(boolean failed) {
        isFailed = failed;
    }

    public int getIssueId() {
        return issueId;
    }

    public void setIssueId(int issueId) {
        this.issueId = issueId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
