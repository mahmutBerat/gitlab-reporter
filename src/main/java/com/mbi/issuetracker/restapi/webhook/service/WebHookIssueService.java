package com.mbi.issuetracker.restapi.webhook.service;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.service.GitlabIssuesService;
import com.mbi.issuetracker.service.GitlabUserService;
import com.mbi.issuetracker.util.DateUtil;
import com.mbi.issuetracker.websocket.service.WebHookSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;

@Service
public class WebHookIssueService {

    private final Logger logger = LoggerFactory.getLogger(WebHookIssueService.class);

    @Autowired
    WebHookSocketService webhookSocketService;

    @Autowired
    GitlabIssuesService issueService;

    @Autowired
    GitlabUserService userService;

    public void inspectIncomingIssueObject(LinkedHashMap issueMap) {
        //This wait may prevent from empty result from
        //issueService.findIssueByProjectAndIssueId for scheduled operation
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("Inspecting incoming gitlab issue event");
        String eventType = (String) issueMap.get("event_type");
        if (eventType.equals("issue")) {
            ReporterIssue issue = parseIssueAttributes((LinkedHashMap) issueMap.get("object_attributes"));
            if (issueService.findIssueByProjectAndIssueId(issue.getProjectId(), issue.getIid()) == null) {
                //persist new issue
                issueService.saveAndFlushReporterIssue(issue);
                logger.info("Gitlab generated new issue persisted. IssueId: {}", issue.getReporterId());
                webhookSocketService.sendIssueEvent("A new issue persisted on reporter app");
            }
        }
    }

    private ReporterIssue parseIssueAttributes(LinkedHashMap attributes) {

        ReporterIssue reporterIssue = new ReporterIssue();
        reporterIssue.setId((Integer) attributes.get("id"));
        reporterIssue.setIid((Integer) attributes.get("iid"));
        String attrDate = (String) attributes.get("created_at");

        Date date = DateUtil.convertToNewFormat(attrDate);
        reporterIssue.setCreatedAt(date);

        ReporterUser reporterUser = userService.getUserByUserId(Long.valueOf((Integer) attributes.get("author_id")));
        reporterIssue.setCreatedBy(reporterUser);

        reporterIssue.setProjectId((Integer) attributes.get("project_id"));
        reporterIssue.setDescription((String) attributes.get("description"));
        reporterIssue.setTitle((String) attributes.get("title"));
        reporterIssue.setWebUrl((String) attributes.get("url"));
        reporterIssue.setUserNotesCount(0);
        reporterIssue.setFailedToSent(false);
        return reporterIssue;
    }
}
