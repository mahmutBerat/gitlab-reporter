package com.mbi.issuetracker.restapi.webhook;

import com.mbi.issuetracker.restapi.webhook.service.WebHookIssueService;
import com.mbi.issuetracker.websocket.service.WebHookSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedHashMap;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api/incoming/gitlab")
public class WebHookAPI {

    private final Logger logger = LoggerFactory.getLogger(WebHookAPI.class);

    @Autowired
    WebHookIssueService webhookIssueService;

    @Autowired
    WebHookSocketService webhookSocketService;

    @PostMapping(path = "/issuesEvents")
    public void issuesEvents(@RequestBody LinkedHashMap issueEventsPost){
        logger.info("Web hook invoked by Gitlab. Gitlab Server Issue Event just came.");
        webhookIssueService.inspectIncomingIssueObject(issueEventsPost);
    }

    @PostMapping(path = "/commentEvents")
    public void commentEvents(){
        logger.info("Web hook invoked by Gitlab. Gitlab Server Note Event just came");
        //TODO create inspectIncomingCommentObject
        webhookSocketService.sendCommentEvent("test message content");
    }
}
