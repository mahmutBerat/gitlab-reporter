package com.mbi.issuetracker.restapi.offline.client;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.IssuesApi;
import org.gitlab4j.api.models.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GitlabIssuesClient {

    private final Logger logger = LoggerFactory.getLogger(GitlabIssuesClient.class);

    private GitlabFactory gitlabFactory = new GitlabFactory();

    private final String gitlabApiExceptionMessage = "Cannot send failed reporter issue. Issue id {}, title {}, Project id {}";

    public Issue postCreateIssue(ReporterIssue reporterIssue, ReporterUser user) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        IssuesApi issuesApi = gitLabApi.getIssuesApi();
        Issue issue = null;
        try {
            issue = issuesApi
                    .createIssue(reporterIssue.getProjectId(), reporterIssue.getTitle(), reporterIssue.getDescription());
        } catch (GitLabApiException e) {
            logger.error(gitlabApiExceptionMessage, reporterIssue.getReporterId(), reporterIssue.getTitle(), reporterIssue.getProjectId());
        }
        return issue;
    }

    public Issue postCloseIssue(ReporterIssue reporterIssue, ReporterUser user) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        IssuesApi issuesApi = gitLabApi.getIssuesApi();
        Issue issue = null;
        try {
            issue = issuesApi
                    .closeIssue(reporterIssue.getProjectId(), reporterIssue.getIid());
        } catch (RuntimeException e) {
            logger.error(e.getMessage() + gitlabApiExceptionMessage, reporterIssue.getReporterId(), reporterIssue.getTitle(), reporterIssue.getProjectId());
        }catch (GitLabApiException e) {
            logger.error(gitlabApiExceptionMessage, reporterIssue.getReporterId(), reporterIssue.getTitle(), reporterIssue.getProjectId());
        }
        return issue;
    }

    public void postDeleteIssue(ReporterIssue reporterIssue, ReporterUser user) throws GitLabApiException {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        IssuesApi issuesApi = gitLabApi.getIssuesApi();
        Issue issue = null;
        try {
            issuesApi
                    .deleteIssue(reporterIssue.getProjectId(), reporterIssue.getIid());
        } catch (RuntimeException e) {
            logger.error(e.getMessage() + gitlabApiExceptionMessage, reporterIssue.getReporterId(), reporterIssue.getTitle(), reporterIssue.getProjectId());
        } catch (GitLabApiException e) {
            logger.error(gitlabApiExceptionMessage, reporterIssue.getReporterId(), reporterIssue.getTitle(), reporterIssue.getProjectId());
            throw new GitLabApiException(e);
        }
    }

}
