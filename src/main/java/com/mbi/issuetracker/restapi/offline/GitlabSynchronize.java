package com.mbi.issuetracker.restapi.offline;

import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.security.GitlabApplicationConfig;
import com.mbi.issuetracker.service.GitlabIssuesService;
import com.mbi.issuetracker.websocket.service.GitlabHealthSocketService;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.HealthCheckApi;
import org.gitlab4j.api.models.HealthCheckInfo;
import org.gitlab4j.api.models.HealthCheckStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class GitlabSynchronize {

    private Logger logger = LoggerFactory.getLogger(GitlabSynchronize.class);

    @Autowired
    GitlabFactory gitlabFactory;

    @Autowired
    GitlabApplicationConfig gitlabApplicationConfig;

    @Autowired
    GitlabIssuesService gitlabIssuesService;

    @Autowired
    GitlabHealthSocketService healthSocketService;

    private String gitlabAddress;
    private String applicationAccessToken;

    @PostConstruct
    public void initParams() {
        gitlabAddress = gitlabApplicationConfig.getGitlabHostAddress();
        applicationAccessToken = gitlabApplicationConfig.getApplicationAccessToken();
    }

    @Scheduled(fixedRate = 20 * 1000)
    public void checkGitlabHealth(){
        logger.info("Checking gitlab connectivity");
        GitLabApi gitLabApi =new GitLabApi(gitlabAddress, applicationAccessToken);
        try{
            HealthCheckApi healthCheckApi = gitLabApi.getHealthCheckApi();
            HealthCheckInfo healthCheckInfo = healthCheckApi.getReadiness(applicationAccessToken);
            if(healthCheckInfo.getDbCheck().getStatus().equals(HealthCheckStatus.OK)){
                logger.info(String.format("Gitlab Server Health check successful. %s", healthCheckInfo.getDbCheck().getStatus().toString()));
                //should send connected info to ui over web socket
                healthSocketService.sendHealthSignal("OK");
            }
        }catch (GitLabApiException e){
            logger.error("Gitlab Health Check failed");
            //should send not connected info to ui over web socket
            healthSocketService.sendHealthSignal("NOT");
        }
    }

}
