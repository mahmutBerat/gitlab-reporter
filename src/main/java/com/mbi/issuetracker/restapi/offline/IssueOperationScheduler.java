package com.mbi.issuetracker.restapi.offline;

import com.mbi.issuetracker.persistence.entity.IssueOperations;
import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.restapi.offline.client.GitlabIssuesClient;
import com.mbi.issuetracker.websocket.service.WebHookSocketService;
import com.mbi.issuetracker.service.GitlabIssueOperationService;
import com.mbi.issuetracker.service.GitlabIssuesService;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IssueOperationScheduler extends GitlabSynchronize {

    private final Logger logger = LoggerFactory.getLogger(IssueOperationScheduler.class);

    @Autowired
    GitlabIssueOperationService operationService;

    @Autowired
    WebHookSocketService webhookSocketService;

    private GitlabIssuesClient gitlabIssuesClient = new GitlabIssuesClient();


    @Autowired
    GitlabIssuesService issuesService;

    //@Scheduled(fixedRate = 10 * 1000)
    public void checkWaitingOperation() {

        logger.info("Checking for waiting operations");
        List<IssueOperations> waitingOperations = operationService.findWaitingOperations();
        for (IssueOperations operation : waitingOperations) {
            logger.info("performing waiting operations operationId {}", operation.getId());
            ReporterIssue reporterIssue = operation.getReporterIssue();
            ReporterUser submitter = operation.getSubmitter();
            boolean deleteOperationSuccess = false;
            Issue issue = null;
            switch (operation.getType()) {
                //TODO 1- Go to gitlab and perform operation
                case CREATE:
                    logger.info("performing create operation for issueProjectId {}, issueId{}", reporterIssue.getProjectId(), reporterIssue.getIid());
                    issue = gitlabIssuesClient.postCreateIssue(reporterIssue, reporterIssue.getCreatedBy());
                    //TODO 2- Persist new Issue info for ReporterIssue
                    if (issue != null) {
                        issuesService.persistSuccessIssue(reporterIssue, issue);
                    }
                    break;
                case CLOSE:
                    logger.info("performing close operation for issueProjectId {}, issueId{}", reporterIssue.getProjectId(), reporterIssue.getIid());
                    issue = gitlabIssuesClient.postCloseIssue(reporterIssue, submitter);
                    //TODO 2- Persist closed Issue info for ReporterIssue
                    if (issue != null) {
                        issuesService.persistSuccessfullyClosedIssue(reporterIssue, issue, submitter);
                    }
                    break;
                case DELETE:
                    try {
                        logger.info("performing delete operation for issueProjectId {}, issueId{}", reporterIssue.getProjectId(), reporterIssue.getIid());
                        gitlabIssuesClient.postDeleteIssue(reporterIssue, submitter);
                        //TODO delete or soft delete issue
                        issuesService.persistSuccessfullyDeletedIssue(reporterIssue, submitter);
                        deleteOperationSuccess = true;
                    } catch (GitLabApiException e) {
                        logger.error("Cannot delete issue from remote. Issue id {}", reporterIssue.getId());
                    }
                    break;
            }

            if (issue != null || deleteOperationSuccess) {
                //TODO UPDATE IssueOperation as SUCCESS status
                operationService.updateIssueOperationAsSuccess(operation);

                logger.info("IssueOperation scheduler completed operation successfully operation : {}", operation.getId());
                webhookSocketService.sendIssueEvent("Need to sync external issue store");
            } else {
                //TODO UPDATE IssueOperation as FAIL status
                operationService.updateIssueOperationAsFail(operation);
                logger.info("issue operation is failed. Previous operation marked as fail. operationId {}", operation.getId());

                //TODO CREATE a new IssueOperation with WAITING status
                operationService.reCreateWaitingOperation(operation);
                logger.info("new waiting state operation created, operationId {}", operation.getId());
            }

        }


    }
}
