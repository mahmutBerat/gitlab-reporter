package com.mbi.issuetracker.restapi.factory;

import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.springframework.stereotype.Component;

@Component
public class GitlabFactory {

    private static final GitLabApi.ApiVersion apiVersion = GitLabApi.ApiVersion.V4;

    private static final String hostUrl = "http://localhost:30080";

    public GitLabApi createAPI() {
        return new GitLabApi(apiVersion, hostUrl, "");
    }

    public GitLabApi createAPI(String privateToken) {
        return new GitLabApi(apiVersion, hostUrl, privateToken);
    }

    public GitLabApi createAPI(AccessTokenResponseDTO accessTokenResponseDTO) {
        return new GitLabApi(apiVersion, hostUrl, Constants.TokenType.ACCESS, accessTokenResponseDTO.getAccess_token()
                , null, null);
    }

    public GitLabApi createAPIByBearerToken(String userAccessToken) {
        return new GitLabApi(apiVersion, hostUrl, Constants.TokenType.ACCESS, userAccessToken
                , null, null);
    }
}
