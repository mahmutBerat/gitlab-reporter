package com.mbi.issuetracker.restapi.factory;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import org.gitlab4j.api.models.Issue;

import java.util.Date;

public class ReporterIssueFactory {

    public ReporterIssue createReporterIssue(IssueDTO issueDTO, ReporterUser reporterUser) {
        ReporterIssue reporterIssue = new ReporterIssue();
        reporterIssue.setProjectId(issueDTO.getProjectId());
        reporterIssue.setCreatedBy(reporterUser);
        reporterIssue.setDescription(issueDTO.getDescription());
        reporterIssue.setTitle(issueDTO.getTitle());
        reporterIssue.setCreatedAt(new Date());
        reporterIssue.setFailedToSent(Boolean.TRUE);
        return reporterIssue;
    }

    public ReporterIssue fillCreatedReporterIssueByGitlabIssue(ReporterIssue reporterIssue, Issue issue) {
        reporterIssue.setId(issue.getId());
        reporterIssue.setIid(issue.getIid());
        reporterIssue.setTitle(issue.getTitle());
        reporterIssue.setDescription(issue.getDescription());
        reporterIssue.setDeleted(Boolean.FALSE);
        reporterIssue.setWebUrl(issue.getWebUrl());
        reporterIssue.setClosedAt(issue.getClosedAt());
        reporterIssue.setUserNotesCount(issue.getUserNotesCount());
        reporterIssue.setFailedToSent(Boolean.FALSE);
        return reporterIssue;
    }

    public ReporterIssue fillClosedReporterIssueByGitlabIssue(ReporterIssue reporterIssue, Issue issue, ReporterUser submitter) {
        reporterIssue = fillCreatedReporterIssueByGitlabIssue(reporterIssue, issue);
        reporterIssue.setClosedBy(submitter);
        return reporterIssue;
    }

    public ReporterIssue fillDeletedReporterIssueByGitlabIssue(ReporterIssue reporterIssue, ReporterUser submitter) {
        reporterIssue.setClosedBy(submitter);
        reporterIssue.setDeleted(Boolean.TRUE);
        return reporterIssue;
    }
}
