package com.mbi.issuetracker.restapi.auth;

import com.mbi.issuetracker.security.GitlabApplicationConfig;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("unused")
@RestController
public class LoginController {

    @Autowired
    GitlabApplicationConfig gitlabApplicationConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = "/login")
    public void getLoginAddress(HttpServletRequest request, HttpServletResponse response) {
        logger.info("login request");
    }

    //Works ok
    @GetMapping(value = "/signinwithgitlab")
    public JSONObject getGitlabTarget() {
        logger.info("Sign In with Gitlab step starts. Returning target Gitlab address ");
        String authorizeUrl = gitlabApplicationConfig.getTargetRFC();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("address", authorizeUrl);
        return jsonObject;
    }

}