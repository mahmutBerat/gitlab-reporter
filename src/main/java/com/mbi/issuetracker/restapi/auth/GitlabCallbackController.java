package com.mbi.issuetracker.restapi.auth;

import com.mbi.issuetracker.exception.ReporterException;
import com.mbi.issuetracker.restapi.dto.AccessTokenDTO;
import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import com.mbi.issuetracker.service.GitlabAuthService;
import com.mbi.issuetracker.service.GitlabUserService;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class GitlabCallbackController {

    @Autowired
    GitlabAuthService authService;

    @Autowired
    GitlabUserService userService;

    @GetMapping(path = "/callback")
    public AccessTokenDTO get(@RequestParam String code, @RequestParam String state) throws ReporterException, GitLabApiException {
        AccessTokenResponseDTO accessTokenResponseDTO = authService.postAuthorization(code);
        if(accessTokenResponseDTO.getAccess_token().isEmpty())
            throw new ReporterException("Access Token is Empty");

        //persist token & create user session
        User user = userService.getUserDetails(accessTokenResponseDTO);
        userService.persistUser(user,accessTokenResponseDTO);
        //return to UI
        return new AccessTokenDTO("OKEY") ;
    }
}
