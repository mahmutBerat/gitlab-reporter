package com.mbi.issuetracker.restapi.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auth")
public class MainAPI {

    private Logger logger = LoggerFactory.getLogger(MainAPI.class);

    @GetMapping(path = "/")
    public String initial(){
        logger.info("Main API initial GET ");
        return "/";
    }
}
