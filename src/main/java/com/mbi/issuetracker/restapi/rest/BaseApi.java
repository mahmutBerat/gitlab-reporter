package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class BaseApi {

    @Autowired
    UserDao userDao;

    ReporterUser getUser(){
        ReporterUser reporterUser = userDao.findByUserName(getUserName()).orElse(null);
        return reporterUser;
    }

    String getUserName(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return (String) auth.getPrincipal();
    }
    String getBearerToken(){
        ReporterUser reporterUser = userDao.findByUserName(getUserName()).orElse(null);
        if (reporterUser != null) {
            return reporterUser.getBearerToken();
        }
        return "";
    }
}
