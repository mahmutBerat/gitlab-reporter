package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.service.GitlabUserService;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/users")
public class GitlabUserAPI {

    @Autowired
    GitlabUserService gitlabUserService;

    @GetMapping(path = "/{userId}")
    public User getCurrentUser(@PathVariable long userId) throws GitLabApiException {
        return gitlabUserService.getUserDetails(userId);
    }
}
