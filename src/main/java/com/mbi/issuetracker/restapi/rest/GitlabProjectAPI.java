package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.persistence.entity.ReporterProject;
import com.mbi.issuetracker.security.SecurityPrincipal;
import com.mbi.issuetracker.service.GitlabProjectsService;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/api/projects")
public class GitlabProjectAPI extends BaseApi {

    @Autowired
    GitlabProjectsService gitlabProjectsService;

    @GetMapping(path = "/fetchGitlabProjects")
    public List<String> fetchGitlabProjects(@AuthenticationPrincipal SecurityPrincipal user, @RequestParam Long userId) throws GitLabApiException {
        return gitlabProjectsService.fetchUserProjects(userId).getProjectList().stream().map(ReporterProject::getName).collect(Collectors.toList());
    }
    @GetMapping(path = "/getUserProjects")
    public List<ReporterProject> getUserProjects(@AuthenticationPrincipal SecurityPrincipal user, @RequestParam Long userId) throws GitLabApiException {
        return gitlabProjectsService.getUserProjects(userId);
    }

}
