package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.persistence.entity.ReporterNote;
import com.mbi.issuetracker.restapi.dto.NoteDTO;
import com.mbi.issuetracker.security.SecurityPrincipal;
import com.mbi.issuetracker.service.GitlabNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping(path = "/api/notes")
public class GitlabNotesAPI extends BaseApi {

    @Autowired
    GitlabNotesService gitlabNotesService;

    @GetMapping
    public List<ReporterNote> getIssueUserNotes(@RequestParam Integer projectId, @RequestParam Integer issueId) {
        return gitlabNotesService.getUserNotes(getBearerToken(), projectId, issueId);
    }

    @GetMapping(path = "/fetchIssueNotes")
    public List<ReporterNote> fetchIssueNotes(@AuthenticationPrincipal SecurityPrincipal securityPrincipal,
                                              @RequestParam Integer projectId, @RequestParam Integer issueId) {
        return gitlabNotesService.fetchIssueNotes(getBearerToken(), projectId, issueId);
    }

    @PostMapping
    public ReporterNote createNote(@RequestBody NoteDTO noteDTO) {
        return gitlabNotesService.createNote(noteDTO, getBearerToken());
    }

    @PostMapping(path = "/deleteNote")
    public ReporterNote deleteNote(@RequestBody NoteDTO noteDTO, HttpServletResponse httpServletResponse) {
        ReporterNote reporterNote = gitlabNotesService.deleteNote(noteDTO, getBearerToken());
        if (reporterNote == null) {
            httpServletResponse.setStatus(503);
        }
        return reporterNote;
    }
}