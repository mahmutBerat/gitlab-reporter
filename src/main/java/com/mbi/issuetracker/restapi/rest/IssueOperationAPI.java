package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.persistence.entity.IssueOperations;
import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.service.GitlabIssueOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/issueOperation")
public class IssueOperationAPI extends BaseApi {

    @Autowired
    GitlabIssueOperationService operationService;

    @GetMapping(path = "/getWaitingOperations")
    public List<IssueOperations> getWaitingOperations(){
        return operationService.findWaitingOperations();
    }

    @PostMapping
    public ReporterIssue createIssue(@RequestBody IssueDTO issueDTO) {
        return operationService.createIssue(issueDTO);
    }

    @PostMapping(path = "/close")
    public ReporterIssue closeIssue(@RequestBody IssueDTO issueDTO) {
        return operationService.closeIssue(issueDTO);
    }

    @PostMapping(path = "/delete")
    public ReporterIssue deleteIssue(@RequestBody IssueDTO issueDTO) {
        return operationService.deleteIssue(issueDTO);
    }
}
