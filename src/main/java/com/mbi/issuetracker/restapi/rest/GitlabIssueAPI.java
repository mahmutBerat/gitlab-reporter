package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.security.SecurityPrincipal;
import com.mbi.issuetracker.service.GitlabIssuesService;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Issue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/issues")
public class GitlabIssueAPI extends BaseApi {

    @Autowired
    GitlabIssuesService gitlabIssuesService;

    @GetMapping
    public List<ReporterIssue> getReporterIssues(@AuthenticationPrincipal SecurityPrincipal securityPrincipal, @RequestParam Integer projectId) throws GitLabApiException {
        return gitlabIssuesService.getGitlabProjectIssues(getUser(), projectId);
    }

    @GetMapping(path = "/fetchFromGitlab")
    public List<Issue> fetchIssues(@AuthenticationPrincipal SecurityPrincipal securityPrincipal, @RequestParam Integer projectId) throws GitLabApiException {
            return gitlabIssuesService.fetchIssues(getUser(), projectId);
    }

    @PostMapping
    public ReporterIssue createIssue(@RequestBody IssueDTO issueDTO) {
        return gitlabIssuesService.createIssue(issueDTO, getUser());
    }

    @PostMapping(path = "/closeIssue")
    public ReporterIssue closeIssue(@RequestBody IssueDTO issueDTO){
        return gitlabIssuesService.closeIssue(issueDTO, getUser());
    }

    @PostMapping(path = "/deleteIssue")
    public ReporterIssue deleteIssue(@RequestBody IssueDTO issueDTO) throws GitLabApiException {
        return gitlabIssuesService.deleteIssue(issueDTO.getProjectId(), issueDTO.getIssueId(), getUser());
    }
}
