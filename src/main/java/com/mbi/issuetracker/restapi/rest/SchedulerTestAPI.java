package com.mbi.issuetracker.restapi.rest;

import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.restapi.offline.IssueOperationScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/schedulerTestApi")
public class SchedulerTestAPI extends BaseApi {

    @Autowired
    IssueOperationScheduler operationScheduler;

    @PostMapping
    public void createIssue(@RequestBody IssueDTO issueDTO) {
        operationScheduler.checkWaitingOperation();
    }
}
