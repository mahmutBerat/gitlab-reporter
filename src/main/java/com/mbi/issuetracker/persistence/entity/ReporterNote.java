package com.mbi.issuetracker.persistence.entity;

import org.gitlab4j.api.models.Note;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "notes")
public class ReporterNote {

    @Id
    private int id;

    private String title;

    private String body;

    private Date createdAt;

    private Integer createdBy;

    private Integer projectId;

    private Integer issueId;

    @Column(name = "is_system_note")
    private boolean isSystemNote;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    ReporterNote(Note note, int projectId, int issueId) {
        if (note != null) {
            this.id = note.getId();
            this.title = note.getTitle();
            this.body = note.getBody();
            this.createdAt = note.getCreatedAt();
            this.createdBy = note.getAuthor().getId();
            this.isSystemNote = note.getSystem();
        }
        this.projectId = projectId;
        this.issueId = issueId;
    }

    public ReporterNote(int id, String title, String body, Date createdAt, Integer createdBy, Integer projectId, Integer issueId, boolean isSystemNote) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.projectId = projectId;
        this.issueId = issueId;
        this.isSystemNote = isSystemNote;
    }

    public ReporterNote() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getIssueId() {
        return issueId;
    }

    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    public boolean isSystemNote() {
        return isSystemNote;
    }

    public void setSystemNote(boolean systemNote) {
        isSystemNote = systemNote;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Boolean getSystemNote() {
        return isSystemNote;
    }

    public void setSystemNote(Boolean systemNote) {
        isSystemNote = systemNote;
    }

    @Override
    public String toString() {
        return "ReporterNote{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }

    public static class ReporterNoteBuilder {

        private Note note;

        private int projectId;

        private int issueId;

        public ReporterNoteBuilder setNote(Note note) {
            this.note = note;
            return this;
        }

        public ReporterNoteBuilder setProjecId(int projectId) {
            this.projectId = projectId;
            return this;
        }

        public ReporterNoteBuilder setIssueId(int issueId) {
            this.issueId = issueId;
            return this;
        }

        public ReporterNote build() {
            return new ReporterNote(note, projectId, issueId);
        }

    }
}
