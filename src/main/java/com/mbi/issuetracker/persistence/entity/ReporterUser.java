package com.mbi.issuetracker.persistence.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class ReporterUser {

    private Long id;

    private Long userId;

    private String userName;

    private String password;

    private String email;

    private String bearerToken;

    private String accessToken;

    private String bio;

    private String name;

    private String avatarUrl;

    private List<ReporterProject> projectList = new ArrayList<>();

    private List<ReporterIssue> issueList = new ArrayList<>();

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "user_id", unique = true)
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBearerToken() {
        return bearerToken;
    }

    public void setBearerToken(String bearerToken) {
        this.bearerToken = bearerToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToMany(fetch = FetchType.LAZY )
    @JoinTable(name = "user_project", joinColumns = {
            @JoinColumn(name = "user_id", nullable = false)
    }, inverseJoinColumns = {@JoinColumn(name = "project_id",
            nullable = false)})
    public List<ReporterProject> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<ReporterProject> projectList) {
        this.projectList = projectList;
    }


    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @OneToMany
    public List<ReporterIssue> getIssueList() {
        return issueList;
    }

    public void setIssueList(List<ReporterIssue> issueList) {
        this.issueList = issueList;
    }

    @Override
    public String toString() {
        return "ReporterUser{" +
                "id=" + id +
                ", userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", bearerToken='" + bearerToken + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", bio='" + bio + '\'' +
                ", name='" + name + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", projectList=" + projectList +
                '}';
    }
}
