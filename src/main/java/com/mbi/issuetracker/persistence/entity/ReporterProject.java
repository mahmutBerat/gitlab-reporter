package com.mbi.issuetracker.persistence.entity;

import javax.persistence.*;

@Entity
@Table(name = "projects", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"project_id"})
})
public class ReporterProject {

    private String description;
    private Integer projectId;
    private String name;
    private String path;
    private Integer starCount;

    public ReporterProject() {
    }

    public ReporterProject(String description, Integer project_id, String name, String path, Integer starCount) {
        this.description = description;
        this.projectId = project_id;
        this.name = name;
        this.path = path;
        this.starCount = starCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Id
    @Column(name = "project_id")
    public Integer getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getStarCount() {
        return starCount;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public void setStarCount(Integer starCount) {
        this.starCount = starCount;
    }

    @Override
    public String toString() {
        return "ReporterProject{" +
                ", description='" + description + '\'' +
                ", projectId=" + projectId +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", starCount=" + starCount +
                '}';
    }
}
