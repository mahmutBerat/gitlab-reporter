package com.mbi.issuetracker.persistence.entity;


import org.gitlab4j.api.models.Issue;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "issue")
public class ReporterIssue {

    private Integer reporterId;
    private Date createdAt;
    private Date closedAt;
    private String description;
    private Integer id;
    private Integer iid;
    private Integer projectId;
    private String title;
    private Integer userNotesCount;
    private String webUrl;
    private Boolean isFailedToSent;
    private Date failedDate;
    private ReporterUser createdBy;
    private ReporterUser closedBy;
    private Boolean isDeleted;

    public ReporterIssue() {
    }

    public ReporterIssue(Issue issue, ReporterUser user) {
        this.createdAt = issue.getCreatedAt();
        this.closedAt = issue.getClosedAt();
        this.description = issue.getDescription();
        this.id = issue.getId();
        this.iid = issue.getIid();
        this.projectId = issue.getProjectId();
        this.title = issue.getTitle();
        this.userNotesCount = issue.getUserNotesCount();
        this.webUrl = issue.getWebUrl();
        this.createdBy = user;
    }

    @Id
    @GeneratedValue
    public Integer getReporterId() {
        return reporterId;
    }

    public void setReporterId(Integer reporterId) {
        this.reporterId = reporterId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(unique = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUserNotesCount() {
        return userNotesCount;
    }

    public void setUserNotesCount(Integer userNotesCount) {
        this.userNotesCount = userNotesCount;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Date getFailedDate() {
        return failedDate;
    }

    public void setFailedDate(Date failedDate) {
        this.failedDate = failedDate;
    }

    public Boolean getFailedToSent() {
        return isFailedToSent;
    }

    public void setFailedToSent(Boolean failedToSent) {
        isFailedToSent = failedToSent;
    }

    @ManyToOne
    @JoinColumn
    public ReporterUser getClosedBy() {
        return closedBy;
    }

    public void setClosedBy(ReporterUser closedBy) {
        this.closedBy = closedBy;
    }

    @ManyToOne
    @JoinColumn
    public ReporterUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(ReporterUser createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "is_deleted")
    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }


    public static class ReporterIssueBuilder {

        private Issue issue;

        private ReporterUser user;

        public ReporterIssueBuilder setIssue(Issue issue){
            this.issue = issue;
            return this;
        }

        public ReporterIssueBuilder setUser(ReporterUser user){
            this.user = user;
            return this;
        }

        public ReporterIssue build(){
            return new ReporterIssue(issue, user);
        }

    }

}
