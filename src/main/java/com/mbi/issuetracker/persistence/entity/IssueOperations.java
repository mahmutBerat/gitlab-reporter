package com.mbi.issuetracker.persistence.entity;

import com.mbi.issuetracker.persistence.IssueOperationStatus;
import com.mbi.issuetracker.persistence.IssueOperationType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "issue_operations")
public class IssueOperations {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private ReporterIssue reporterIssue;

    private IssueOperationType type;

    private Date creation_date;

    private Boolean isExecuted;

    @Enumerated(EnumType.STRING)
    private IssueOperationStatus status;

    @ManyToOne
    @JoinColumn(name = "submitter_id")
    private ReporterUser submitter;

    public ReporterUser getSubmitter() {
        return submitter;
    }

    public void setSubmitter(ReporterUser submitter) {
        this.submitter = submitter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ReporterIssue getReporterIssue() {
        return reporterIssue;
    }

    public void setReporterIssue(ReporterIssue reporterIssue) {
        this.reporterIssue = reporterIssue;
    }

    public IssueOperationType getType() {
        return type;
    }

    public void setType(IssueOperationType type) {
        this.type = type;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public IssueOperationStatus getStatus() {
        return status;
    }

    public void setStatus(IssueOperationStatus status) {
        this.status = status;
    }

    public Boolean getExecuted() {
        return isExecuted;
    }

    public void setExecuted(Boolean executed) {
        isExecuted = executed;
    }

}
