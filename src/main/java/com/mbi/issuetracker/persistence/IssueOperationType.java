package com.mbi.issuetracker.persistence;

public enum IssueOperationType {

    CREATE,
    CLOSE,
    DELETE;

    IssueOperationType() {
    }
}
