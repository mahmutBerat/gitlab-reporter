package com.mbi.issuetracker.persistence;

public enum IssueOperationStatus {

    SUCCESS,
    FAIL,
    WAITING;

    IssueOperationStatus() {
    }
}
