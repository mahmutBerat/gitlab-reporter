package com.mbi.issuetracker.persistence.repository;

import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IssueDao extends JpaRepository<ReporterIssue, Integer> {

    @Query("Select ri from ReporterIssue ri where failedToSent= TRUE ")
    List<ReporterIssue> findAllByIsFailedToSent();

    @Query("Select ri from ReporterIssue ri where id=?1")
    ReporterIssue findByGitlabId(Integer id);

    List<ReporterIssue> findAllByClosedAtIsNullAndDeletedFalseOrDeletedIsNull();

    @Query("Select ri from ReporterIssue ri where ri.projectId=?1 and ri.iid=?2")
    ReporterIssue findByGitlabProjectAndIssueId(int projectId, int iid);
}
