package com.mbi.issuetracker.persistence.repository;

import com.mbi.issuetracker.persistence.entity.ReporterProject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProjectDao extends JpaRepository<ReporterProject, Integer> {

    Optional<ReporterProject> findByProjectId(Integer id);

}
