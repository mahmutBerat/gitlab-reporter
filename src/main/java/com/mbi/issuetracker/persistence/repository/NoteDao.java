package com.mbi.issuetracker.persistence.repository;

import com.mbi.issuetracker.persistence.entity.ReporterNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NoteDao extends JpaRepository<ReporterNote, Integer> {

    @Query("Select rn from ReporterNote rn where rn.isDeleted = false and rn.isSystemNote = false")
    List<ReporterNote> findAllSystemNoteAndDeletedIsFalse();
}
