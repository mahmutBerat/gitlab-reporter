package com.mbi.issuetracker.persistence.repository;

import com.mbi.issuetracker.persistence.entity.IssueOperations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IssueOperationsDao extends JpaRepository<IssueOperations, Long> {

    @Query("select io from IssueOperations io where io.status = com.mbi.issuetracker.persistence.IssueOperationStatus.WAITING order by io.creation_date")
    List<IssueOperations> findByWaitingStatusAndAndExecutedFalse();
}
