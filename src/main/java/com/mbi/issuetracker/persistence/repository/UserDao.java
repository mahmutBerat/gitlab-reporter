package com.mbi.issuetracker.persistence.repository;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserDao extends JpaRepository<ReporterUser, Long> {

    Optional<ReporterUser> findByUserName(String name);

    ReporterUser findByUserId(Long userId);
}
