package com.mbi.issuetracker.exception;

public class ReporterException extends RuntimeException {

    public ReporterException() {
    }

    public ReporterException(String message) {
        super(message);
    }

    public ReporterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReporterException(Throwable cause) {
        super(cause);
    }

    public ReporterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
