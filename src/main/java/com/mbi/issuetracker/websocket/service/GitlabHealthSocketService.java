package com.mbi.issuetracker.websocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class GitlabHealthSocketService {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @SendTo("/gitlab/health")
    public void sendHealthSignal(String message) {
        simpMessagingTemplate.convertAndSend("/gitlab/health", message);
    }

}
