package com.mbi.issuetracker.websocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class WebHookSocketService {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @SendTo("/webhook/note")
    public void sendCommentEvent(String message) {
        simpMessagingTemplate.convertAndSend("/webhook/note", message);
    }

    @SendTo("/webhook/issue")
    public void sendIssueEvent(String message) {
        simpMessagingTemplate.convertAndSend("/webhook/issue", message);
    }

}
