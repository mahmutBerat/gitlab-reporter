package com.mbi.issuetracker.security;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GitlabApplicationConfig {

    @Value("${gitlab.application.id}")
    private String applicationId;
    @Value("${gitlab.application.secret.key}")
    private String secretKey;

    @Value("${server.runningIp}")
    private String serverIP;

    @Value("${gitlab.server.accessToken}")
    private String applicationAccessToken;

    //private String redirectURL = "http://192.168.201.87:8090/callback";
    private String redirectURL = getRedirectUrl();

    @Value("${gitlab.server.host}")
    private String gitlabHostAddress = "http://localhost:30080";

    public String getTarget() {
        return String.format("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=token", gitlabHostAddress, applicationId, getRedirectUrl());
    }

    public String getTargetRFC(){
        return getTargetRFC(getHashcode());
    }

    public String getTargetRFC(String hashCode) {
        return String.format("%s/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=%s",
                getTarget(), applicationId, getRedirectUrl(), hashCode);
    }

    public String getHashcode() {
        return Hashing.sha1().hashString("password", Charsets.UTF_8).toString();
    }

    public String getRedirectUrl() {
        return String.format("http://%s:8090/callback", serverIP);
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getServerIP() {
        return serverIP;
    }

    public String getGitlabHostAddress() {
        return gitlabHostAddress;
    }

    public String getApplicationAccessToken() {
        return applicationAccessToken;
    }
}
