package com.mbi.issuetracker.security;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) {
        ReporterUser reporterUser = userDao.findByUserName(username).orElse(null);
        if (reporterUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new SecurityPrincipal(reporterUser);
    }
}