package com.mbi.issuetracker.security;

public class SecurityConstants {
    private SecurityConstants() {
        throw new IllegalStateException("Utility class");
    }

    static final String SECRET = "SecretKeyToGenJWTs";
    static final long EXPIRATION_TIME = 864_000_000; // 10 days
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
}

