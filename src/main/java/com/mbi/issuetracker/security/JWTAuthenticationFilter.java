package com.mbi.issuetracker.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.mbi.issuetracker.exception.ReporterException;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        try {
            ReporterUser credential = new ObjectMapper()
                    .readValue(request.getInputStream(), ReporterUser.class);

                SecurityPrincipal securityPrincipal = new SecurityPrincipal(credential);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            securityPrincipal,
                            credential.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new ReporterException("Access Token is Empty");
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityPrincipal securityPrincipal = (SecurityPrincipal) authResult.getPrincipal();

        String token = JWT.create()
                .withSubject((securityPrincipal.getUsername()))
                .withExpiresAt(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SecurityConstants.SECRET.getBytes()));
        writeResponse(response, securityPrincipal, token);
    }

    private void writeResponse(HttpServletResponse response, SecurityPrincipal securityPrincipal, String token) throws IOException {
        response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
        response.setCharacterEncoding("UTF-8");

        ReporterUser reporterUser = securityPrincipal.getReporterUser();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.createObjectNode();
        ((ObjectNode) jsonNode).put(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
        ((ObjectNode) jsonNode).put("userId", reporterUser.getUserId());
        ((ObjectNode) jsonNode).put("userName", reporterUser.getUserName());
        ((ObjectNode) jsonNode).put("avatarUrl", reporterUser.getAvatarUrl());
        ((ObjectNode) jsonNode).put("bio", reporterUser.getBio());
        ((ObjectNode) jsonNode).put("fullName",reporterUser.getName());
        response.getWriter().write(jsonNode.toString());
    }
}
