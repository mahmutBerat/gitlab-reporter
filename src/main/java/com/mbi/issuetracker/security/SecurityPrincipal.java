package com.mbi.issuetracker.security;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class SecurityPrincipal implements UserDetails {

    private Set<GrantedAuthority> authorities;

    private ReporterUser reporterUser;

    public SecurityPrincipal(ReporterUser reporterUser) {
        this.reporterUser = reporterUser;
        authorities = Collections.emptySet();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return reporterUser.getPassword();
    }

    @Override
    public String getUsername() {
        return reporterUser.getUserName();
    }

    public ReporterUser getReporterUser() {
        return reporterUser;
    }

    public long getUserId() {
        return reporterUser.getId();
    }

    public String getBearerToken() {
        return reporterUser.getBearerToken();
    }

    public void setReporterUser(ReporterUser reporterUser) {
        this.reporterUser = reporterUser;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
