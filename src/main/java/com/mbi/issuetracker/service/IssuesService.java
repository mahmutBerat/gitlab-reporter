package com.mbi.issuetracker.service;

import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import org.json.simple.JSONArray;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class IssuesService {

    /**
     * curl --header "Authorization: Bearer response.getAccessToken()" http://localhost:30080/api/v4/issues
     * @param tokenResponse
     * @return
     */
    public String getIssues(AccessTokenResponseDTO tokenResponse){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.add("Authorization", "Bearer " + tokenResponse.getAccess_token());

        HttpEntity<String> request = new HttpEntity<>("parameters",httpHeaders);

        ResponseEntity<JSONArray> response = restTemplate.exchange("http://localhost:30080/api/v4/issues", HttpMethod.GET,
                request, JSONArray.class);
        return response.getBody().toString();
    }


}
