package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.UserDao;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.UserApi;
import org.gitlab4j.api.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class GitlabUserService {

    @Autowired
    GitlabFactory gitlabFactory;

    @Autowired
    UserDao userDao;

    @Autowired
    PasswordEncoder passwordEncoder;

    public User getUserDetails(Long id) throws GitLabApiException {
        ReporterUser reporterUser = userDao.findById(id).orElse(null);
        if (reporterUser != null) {
            return getUserDetails(reporterUser.getAccessToken());
        }
        return null;
    }

    public User getUserDetails(String privateToken) throws GitLabApiException {
        GitLabApi gitLabApi = gitlabFactory.createAPI(privateToken);
        UserApi userApi = gitLabApi.getUserApi();
        return userApi.getCurrentUser();
    }

    public User getUserDetails(AccessTokenResponseDTO accessTokenResponseDTO) throws GitLabApiException {
        GitLabApi gitLabApi = gitlabFactory.createAPI(accessTokenResponseDTO);
        UserApi userApi = gitLabApi.getUserApi();
        return userApi.getCurrentUser();
    }

    public void persistUser(User user, AccessTokenResponseDTO accessTokenResponseDTO) {
        ReporterUser reporterUser = new ReporterUser();
        reporterUser.setUserName(user.getUsername());
        reporterUser.setUserId(Long.valueOf(user.getId()));
        reporterUser.setEmail(user.getEmail());
        reporterUser.setName(user.getName());
        reporterUser.setBio(user.getBio());
        reporterUser.setAvatarUrl(user.getAvatarUrl());
        reporterUser.setBearerToken(accessTokenResponseDTO.getAccess_token());
        reporterUser.setPassword(passwordEncoder.encode("111"));
        userDao.save(reporterUser);
    }

    public ReporterUser getUser(Long id){
        return userDao.findById(id).orElse(null);
    }
    public ReporterUser getUserByName(String name){
        return userDao.findByUserName(name).orElse(null);
    }

    public ReporterUser getUserByUserId(Long id) {
        return userDao.findByUserId(id);
    }
}
