package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.IssueOperationStatus;
import com.mbi.issuetracker.persistence.IssueOperationType;
import com.mbi.issuetracker.persistence.entity.IssueOperations;
import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.IssueOperationsDao;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.restapi.factory.ReporterIssueFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class GitlabIssueOperationService {


    @Autowired
    GitlabIssuesService gitlabIssuesService;

    @Autowired
    IssueOperationsDao issueOperationsDao;

    @Autowired
    GitlabUserService userService;

    ReporterIssueFactory reporterIssueFactory = new ReporterIssueFactory();


    public List<IssueOperations> findWaitingOperations() {
        return issueOperationsDao.findByWaitingStatusAndAndExecutedFalse();
    }

    public ReporterIssue createIssue(IssueDTO issueDTO) {
        ReporterIssue reporterIssue = createReporterIssue(issueDTO);
        addCreateOperation(reporterIssue);
        return reporterIssue;
    }

    public ReporterIssue closeIssue(IssueDTO issueDTO) {
        ReporterIssue reporterIssue = gitlabIssuesService.findIssueByReporterId(issueDTO.getReporterId());
        ReporterUser submitter = userService.getUserByUserId(issueDTO.getUserId());
        addCloseOperation(reporterIssue, submitter);
        return reporterIssue;
    }

    public ReporterIssue deleteIssue(IssueDTO issueDTO) {
        ReporterIssue reporterIssue = gitlabIssuesService.findIssueByReporterId(issueDTO.getReporterId());
        ReporterUser submitter = userService.getUserByUserId(issueDTO.getUserId());
        addDeleteOperation(reporterIssue,submitter);
        return reporterIssue;
    }

    public ReporterIssue createReporterIssue(IssueDTO issueDTO) {
        ReporterUser reporterUser = userService.getUserByUserId(issueDTO.getUserId());
        ReporterIssue reporterIssue = reporterIssueFactory.createReporterIssue(issueDTO, reporterUser);
        return gitlabIssuesService.saveAndFlushReporterIssue(reporterIssue);
    }

    public IssueOperations reCreateWaitingOperation(IssueOperations prevOperation) {
        IssueOperations operations = new IssueOperations();
        operations.setReporterIssue(prevOperation.getReporterIssue());
        operations.setStatus(IssueOperationStatus.WAITING);
        operations.setType(prevOperation.getType());
        operations.setCreation_date(new Date());
        operations.setSubmitter(prevOperation.getSubmitter());
        return issueOperationsDao.saveAndFlush(operations);
    }

    public IssueOperations updateIssueOperationAsSuccess(IssueOperations operation) {
        operation.setStatus(IssueOperationStatus.SUCCESS);
        operation.setExecuted(Boolean.TRUE);
        return issueOperationsDao.saveAndFlush(operation);
    }

    public IssueOperations updateIssueOperationAsFail(IssueOperations operation) {
        operation.setStatus(IssueOperationStatus.FAIL);
        operation.setExecuted(Boolean.TRUE);
        return issueOperationsDao.saveAndFlush(operation);
    }

    //TODO just try to perform template method design pattern here
    public IssueOperations addCreateOperation(ReporterIssue reporterIssue) {
        IssueOperations operations = generateDefaultIssueOperation(reporterIssue);
        operations.setSubmitter(reporterIssue.getCreatedBy());
        operations.setType(IssueOperationType.CREATE);
        return issueOperationsDao.saveAndFlush(operations);
    }

    public IssueOperations addCloseOperation(ReporterIssue reporterIssue, ReporterUser submitter) {
        IssueOperations operations = generateDefaultIssueOperation(reporterIssue);
        operations.setSubmitter(submitter);
        operations.setType(IssueOperationType.CLOSE);
        return issueOperationsDao.saveAndFlush(operations);
    }

    public IssueOperations addDeleteOperation(ReporterIssue reporterIssue, ReporterUser submitter) {
        IssueOperations operations = generateDefaultIssueOperation(reporterIssue);
        operations.setSubmitter(submitter);
        operations.setType(IssueOperationType.DELETE);
        return issueOperationsDao.saveAndFlush(operations);
    }

    private IssueOperations generateDefaultIssueOperation(ReporterIssue reporterIssue) {
        IssueOperations operations = new IssueOperations();
        operations.setCreation_date(new Date());
        operations.setReporterIssue(reporterIssue);
        operations.setStatus(IssueOperationStatus.WAITING);
        return operations;
    }
}
