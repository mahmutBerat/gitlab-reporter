package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterProject;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.ProjectDao;
import com.mbi.issuetracker.persistence.repository.UserDao;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class GitlabProjectsService {

    private final Logger logger = LoggerFactory.getLogger(GitlabProjectsService.class);

    @Autowired
    GitlabFactory gitlabFactory;

    @Autowired
    GitlabUserService userService;

    @Autowired
    UserDao userDao;

    @Autowired
    ProjectDao projectDao;

    @Transactional
    public List<ReporterProject> getUserProjects(Long id) {
        ReporterUser reporterUser = fetchUserProjects(id);
        return reporterUser.getProjectList();
    }

    public ReporterUser fetchUserProjects(Long id) {
        ReporterUser reporterUser = userService.getUserByUserId(id);
        List<Project> projects = null;
        try {
            projects = getProjects(reporterUser);
        } catch (GitLabApiException e) {
            logger.error(e.getMessage());
        }
        //Persist user projects
        if (projects != null) {
            List<ReporterProject> newProjectsList = saveProjects(projects);
            reporterUser.setProjectList(newProjectsList);
        }
        userDao.save(reporterUser);

        return reporterUser;
    }

    private List<ReporterProject> saveProjects(List<Project> projects) {
        List<ReporterProject> projectList = toReporterProjects(projects);
        for (ReporterProject reporterProject : projectList) {
                projectDao.saveAndFlush(reporterProject);
        }
        return projectList;
    }

    //TODO fix this
    private List<ReporterProject> toReporterProjects(List<Project> projects) {
        Function<Project, ReporterProject> toReporterProjects = new Function<Project, ReporterProject>() {
            public ReporterProject apply(Project project) {
                ReporterProject reporterProject = new ReporterProject();
                reporterProject.setProjectId(project.getId());
                reporterProject.setDescription(project.getDescription());
                reporterProject.setName(project.getName());
                reporterProject.setPath(project.getPath());
                reporterProject.setStarCount(project.getStarCount());

                return reporterProject;
            }
        };
        return projects.stream().map(toReporterProjects).collect(Collectors.toList());
    }

    public List<Project> fetchUserProjects(String name) throws GitLabApiException {
        ReporterUser reporterUser = userService.getUserByName(name);
        return getProjects(reporterUser);
    }

    private List<Project> getProjects(ReporterUser reporterUser) throws GitLabApiException {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(reporterUser.getBearerToken());
        return gitLabApi.getProjectApi().getMemberProjects();
    }


}
