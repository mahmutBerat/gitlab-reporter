package com.mbi.issuetracker.service;

import com.mbi.issuetracker.persistence.entity.ReporterNote;
import com.mbi.issuetracker.persistence.repository.NoteDao;
import com.mbi.issuetracker.restapi.dto.NoteDTO;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Note;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GitlabNotesService {

    private final Logger logger = LoggerFactory.getLogger(GitlabNotesService.class);

    @Autowired
    GitlabFactory gitlabFactory;

    @Autowired
    NoteDao noteDao;

    public List<ReporterNote> getUserNotes(String bearerToken, int projectId, int issueId) {
        List<ReporterNote> reporterNotes = fetchIssueNotes(bearerToken, projectId, issueId);
        if (reporterNotes != null) {
            return reporterNotes.stream().filter(reporterNote -> !reporterNote.isSystemNote()).collect(Collectors.toList());
        }
        return noteDao.findAllSystemNoteAndDeletedIsFalse();
    }

    public List<ReporterNote> fetchIssueNotes(String bearerToken, int projectId, int issueId) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(bearerToken);

        List<Note> gitlabNotes = null;
        try {
            gitlabNotes = gitLabApi.getNotesApi().getIssueNotes(projectId, issueId);
        } catch (GitLabApiException e) {
            logger.error(e.getMessage());
        }
        List<ReporterNote> reporterNotes = null;
        if (gitlabNotes != null) {
            reporterNotes = gitlabNotes.stream().map(note -> toReporterNote(note, projectId, issueId)).collect(Collectors.toList());
            reporterNotes.forEach(reporterNote -> noteDao.save(reporterNote));
        }
        return reporterNotes;
    }

    private static ReporterNote toReporterNote(Note note, int projectId, int issueId) {
        return new ReporterNote.ReporterNoteBuilder()
                .setIssueId(issueId)
                .setProjecId(projectId)
                .setNote(note)
                .build();
    }

    public ReporterNote createNote(NoteDTO noteDTO, String bearerToken) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(bearerToken);

        Note note = postCreateNote(noteDTO, gitLabApi);
        if(note!=null){
            return toReporterNote(note, noteDTO.getProjectId(), noteDTO.getIssueId());
        }
        return null;
    }

    private Note postCreateNote(NoteDTO noteDTO, GitLabApi gitLabApi){
        try {
            return gitLabApi.getNotesApi().createIssueNote(noteDTO.getProjectId(), noteDTO.getIssueId(), noteDTO.getDescription());
        } catch (GitLabApiException e) {
            logger.error("error occured while trying to delete note");
        }
        return null;
    }

    public ReporterNote deleteNote(NoteDTO noteDTO, String bearerToken) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(bearerToken);

        if(postDeleteNote(noteDTO, gitLabApi)){
            ReporterNote reporterNote = noteDao.getOne(noteDTO.getNoteId());
            reporterNote.setDeleted(Boolean.TRUE);
            return noteDao.saveAndFlush(reporterNote);
        }
        return null;
    }

    private boolean postDeleteNote(NoteDTO noteDTO, GitLabApi gitLabApi) {
        try {
            gitLabApi.getNotesApi().deleteIssueNote(noteDTO.getProjectId(), noteDTO.getIssueId(), noteDTO.getNoteId());
        } catch (GitLabApiException e) {
            logger.error("error occurred while trying to delete note");
            return false;
        }
        return true;
    }
}
