package com.mbi.issuetracker.service;

import com.mbi.issuetracker.exception.ReporterException;
import com.mbi.issuetracker.persistence.entity.ReporterIssue;
import com.mbi.issuetracker.persistence.entity.ReporterUser;
import com.mbi.issuetracker.persistence.repository.IssueDao;
import com.mbi.issuetracker.restapi.dto.IssueDTO;
import com.mbi.issuetracker.restapi.factory.GitlabFactory;
import com.mbi.issuetracker.restapi.factory.ReporterIssueFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Strings;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.IssuesApi;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class GitlabIssuesService {

    private final Logger logger = LoggerFactory.getLogger(GitlabIssuesService.class);

    @Autowired
    GitlabFactory gitlabFactory;

    @Autowired
    GitlabUserService gitlabUserService;

    @Autowired
    IssueDao issueDao;

    private ReporterIssueFactory reporterIssueFactory = new ReporterIssueFactory();

    public List<ReporterIssue> getGitlabProjectIssues(ReporterUser reporterUser, Integer projectId) {
        List<Issue> issues = fetchIssues(reporterUser, projectId);
        if (issues != null) {
            persistNewIssues(issues);
        }
        return findOpenReporterIssues();
    }

    private List<ReporterIssue> findOpenReporterIssues() {
        return issueDao.findAllByClosedAtIsNullAndDeletedFalseOrDeletedIsNull();
    }

    public List<Issue> fetchIssues(ReporterUser user, Integer projectId) {
        if (!Strings.isNullOrEmpty(user.getBearerToken())) {
            GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
            IssuesApi gitlabIssueAPI = gitLabApi.getIssuesApi();
            try {
                return gitlabIssueAPI.getIssues(projectId, new IssueFilter());
            } catch (GitLabApiException e) {
                logger.error("Cannot fetch issues from Gitlab server");
            }
        }
        return Collections.emptyList();
    }

    private ReporterIssue buildReporterIssue(Issue issue) {
        ReporterUser reporterUser = gitlabUserService.getUserByUserId(Long.valueOf(issue.getAuthor().getId()));
        return new ReporterIssue.ReporterIssueBuilder()
                .setIssue(issue)
                .setUser(reporterUser)
                .build();
    }

    private void updateReporterIssue(ReporterIssue reporterIssue, Issue issue) {
        reporterIssue.setId(issue.getId());
        reporterIssue.setIid(issue.getIid());
        reporterIssue.setTitle(issue.getTitle());
        reporterIssue.setDescription(issue.getDescription());
        reporterIssue.setUserNotesCount(issue.getUserNotesCount());
        reporterIssue.setWebUrl(issue.getWebUrl());
        reporterIssue.setClosedAt(issue.getClosedAt());
        reporterIssue.setFailedToSent(Boolean.FALSE);
    }

    @Transactional
    public void persistNewIssues(List<Issue> issues) {
        for (Issue issue : issues) {
            ReporterUser reporterUser = gitlabUserService.getUserByUserId(Long.valueOf(issue.getAuthor().getId()));
            ReporterIssue reporterIssue = issueDao.findByGitlabId(issue.getId());
            if (reporterIssue != null) {
                //update
                updateReporterIssue(reporterIssue, issue);
            } else {
                //save initial
                reporterIssue = buildReporterIssue(issue);
                reporterIssue.setCreatedBy(reporterUser);
            }
            saveAndFlushReporterIssue(reporterIssue);
        }
    }

    public ReporterIssue saveAndFlushReporterIssue(ReporterIssue reporterIssue) {
        return issueDao.saveAndFlush(reporterIssue);
    }

    public ReporterIssue createIssue(IssueDTO issueDTO, ReporterUser user) {
        if (!Strings.isNullOrEmpty(user.getBearerToken())) {
            Issue issue = postGitlabIssue(issueDTO, user);
            ReporterIssue reporterIssue;
            if (issue == null) {
                reporterIssue = createFailedReporterIssue(issueDTO, user);
            } else {
                reporterIssue = buildReporterIssue(issue);
            }
            return saveAndFlushReporterIssue(reporterIssue);
        }
        throw new ReporterException("Access Token is Empty");
    }

    public Issue postGitlabIssue(IssueDTO issueDTO, ReporterUser user) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        Issue issue = null;
        try {
            issue = gitLabApi.getIssuesApi().createIssue(issueDTO.getProjectId(), issueDTO.getTitle(), issueDTO.getDescription());
        } catch (GitLabApiException e) {
            logger.error("Cannot send failed reporter issue. Issue id {}, title {}, Project id {}", issueDTO.getReporterId(), issueDTO.getTitle(), issueDTO.getProjectId());
        }
        return issue;
    }

    private ReporterIssue createFailedReporterIssue(IssueDTO issueDTO, ReporterUser reporterUser) {
        ReporterIssue failedReporterIssue = new ReporterIssue();
        //TODO please fix this check
        if (issueDTO.getReporterId() != -1) {
            failedReporterIssue.setReporterId(issueDTO.getReporterId());
        }
        failedReporterIssue.setCreatedBy(reporterUser);
        failedReporterIssue.setFailedToSent(Boolean.TRUE);
        failedReporterIssue.setFailedDate(new Date());
        failedReporterIssue.setDescription(issueDTO.getDescription());
        failedReporterIssue.setDescription(issueDTO.getTitle());
        failedReporterIssue.setProjectId(issueDTO.getProjectId());
        failedReporterIssue.setTitle(issueDTO.getTitle());
        return failedReporterIssue;
    }

    @VisibleForTesting
    List<ReporterIssue> getFailedIssues() {
        return issueDao.findAllByIsFailedToSent();
    }

    public ReporterIssue closeIssue(IssueDTO issueDTO, ReporterUser user) {
        Issue issue = postCloseIssue(issueDTO, user);
        ReporterIssue reporterIssue = issueDao.findByGitlabId(issue.getId());
        updateReporterIssue(reporterIssue, issue);
        return saveAndFlushReporterIssue(reporterIssue);
    }

    private Issue postCloseIssue(IssueDTO issueDTO, ReporterUser user) {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        Issue issue = null;
        try {
            issue = gitLabApi.getIssuesApi().closeIssue(issueDTO.getProjectId(), issueDTO.getIssueId());
        } catch (GitLabApiException e) {
            logger.error(e.getMessage());
        }
        return issue;
    }

    public ReporterIssue findIssueByIssueId(int gitlabId) {
        return issueDao.findByGitlabId(gitlabId);
    }

    public ReporterIssue findIssueByProjectAndIssueId(int projectId, int issueId) {
        return issueDao.findByGitlabProjectAndIssueId(projectId, issueId);
    }

    public ReporterIssue deleteIssue(int projectId, int issueId, ReporterUser user) throws GitLabApiException {
        postDeleteIssue(projectId, issueId, user);
        ReporterIssue reporterIssue = findIssueByProjectAndIssueId(projectId, issueId);
        reporterIssue.setDeleted(Boolean.TRUE);
        reporterIssue.setCreatedAt(new Date());
        reporterIssue.setClosedBy(user);
        return saveAndFlushReporterIssue(reporterIssue);
    }

    private void postDeleteIssue(int projectId, int issueId, ReporterUser user) throws GitLabApiException {
        GitLabApi gitLabApi = gitlabFactory.createAPIByBearerToken(user.getBearerToken());
        gitLabApi.getIssuesApi().deleteIssue(projectId, issueId);
    }

    public ReporterIssue persistSuccessIssue(ReporterIssue reporterIssue, Issue issue) {
        reporterIssue = reporterIssueFactory.fillCreatedReporterIssueByGitlabIssue(reporterIssue, issue);
        return saveAndFlushReporterIssue(reporterIssue);
    }

    public ReporterIssue persistSuccessfullyClosedIssue(ReporterIssue reporterIssue, Issue issue, ReporterUser reporterUser) {
        reporterIssue = reporterIssueFactory.fillClosedReporterIssueByGitlabIssue(reporterIssue, issue, reporterUser);
        return saveAndFlushReporterIssue(reporterIssue);
    }

    public ReporterIssue persistSuccessfullyDeletedIssue(ReporterIssue reporterIssue, ReporterUser submitter) {
        reporterIssue = reporterIssueFactory.fillDeletedReporterIssueByGitlabIssue(reporterIssue, submitter);
        return saveAndFlushReporterIssue(reporterIssue);
    }

    public ReporterIssue findIssueByReporterId(int reporterId) {
        return issueDao.findById(reporterId).orElse(null);
    }
}
