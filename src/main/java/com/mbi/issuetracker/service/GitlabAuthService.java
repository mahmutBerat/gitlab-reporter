package com.mbi.issuetracker.service;

import com.mbi.issuetracker.restapi.dto.AccessTokenResponseDTO;
import com.mbi.issuetracker.restapi.entity.AccessTokenRequester;
import com.mbi.issuetracker.security.GitlabApplicationConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GitlabAuthService {

    @Autowired
    GitlabApplicationConfig gitlabApplicationConfig;

    /**
     * parameters =
     * 'client_id=APP_ID&client_secret=APP_SECRET&code=RETURNED_CODE&grant_type=authorization_code&redirect_uri=REDIRECT_URI'
     * then
     * curl --header "Authorization: Bearer response.getAccessToken()" http://localhost:30080/api/v4/issues
     * @return
     */
    public AccessTokenResponseDTO postAuthorization(String code){
        String targetURL = "http://localhost:30080/oauth/token";
        RestTemplate restTemplate = new RestTemplate();
        AccessTokenRequester accessTokenRequester = createAccessTokenRequester(code);

        HttpEntity<AccessTokenRequester> request = new HttpEntity<>(accessTokenRequester);
        ResponseEntity<AccessTokenResponseDTO> response = restTemplate.exchange(targetURL, HttpMethod.POST, request, AccessTokenResponseDTO.class);
        AccessTokenResponseDTO accessTokenResponseDTO = response.getBody();
        return accessTokenResponseDTO;
    }

    private AccessTokenRequester createAccessTokenRequester(String code) {
        AccessTokenRequester accessTokenRequester = new AccessTokenRequester();
        accessTokenRequester.setClient_id(gitlabApplicationConfig.getApplicationId());
        accessTokenRequester.setClient_secret(gitlabApplicationConfig.getSecretKey());
        accessTokenRequester.setCode(code);
        accessTokenRequester.setGrant_type("authorization_code");
        accessTokenRequester.setRedirect_uri(String.format("http://%s:8090/callback", gitlabApplicationConfig.getServerIP()));
        return accessTokenRequester;
    }

}
