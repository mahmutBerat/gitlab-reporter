# Gitlab Reporter Application Server Side Development 

 - works with internal Gitlab CE. Gitlab CE can be installed on a docker instance 

---

**docker related commands** 
 > ```docker run --detach --name gitlab --hostname gitlab.example.com --publish 30080:30080 --publish 30022:22 --env GITLAB_OMNIBUS_CONFIG="external_url 'http://gitlab.example.com:30080'; gitlab_rails['gitlab_shell_ssh_port']=30022;" gitlab/gitlab-ce:9.1.0-ce.0```

 > ```docker container ls```

 > ``` docker start gitlab ```
 
 > ``` ddocker stop gitlab```
 
 > ``` ddocker logs -f gitlab```
 
---

**gitlab configurations**
 > http://localhost:30080/root/my-project/settings/integrations with root user or project owner

**test users**

root, developer_1, developer_2



**Gitlab API doc**

https://docs.gitlab.com/ce/api/


https://docs.gitlab.com/ce/api/users.html

**sonar
> mvn sonar:sonar \
    -Dsonar.projectKey=reporter \
    -Dsonar.host.url=http://localhost:9000 \
    -Dsonar.login=15ceedd1c1dbc383dd04a42f776ae53add3b0a75